// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <string>

#pragma comment(linker,"/export:A=_A.F,@1")

typedef int(__stdcall* MSGBOXAAPI)(IN HWND hWnd,
    IN LPCSTR lpText, IN LPCSTR lpCaption,
    IN UINT uType, IN WORD wLanguageId, IN DWORD dwMilliseconds);

int MessageBoxTimeoutA(IN HWND hWnd, IN LPCSTR lpText,
    IN LPCSTR lpCaption, IN UINT uType,
    IN WORD wLanguageId, IN DWORD dwMilliseconds);

DWORD WINAPI MainThread(HMODULE m)
{
    wchar_t buffer[MAX_PATH] = { 0 };
    GetModuleFileName(NULL, buffer, MAX_PATH);
    std::wstring::size_type pos = std::wstring(buffer).find_last_of(L"\\/");
    std::wstring path = std::wstring(buffer).substr(0, pos);
    path.append(L"\\.a");

    HANDLE f = CreateFileW(path.c_str(), 0, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_HIDDEN, NULL);

    if (f != NULL && GetLastError() != ERROR_FILE_EXISTS) {
        HMODULE user32 = GetModuleHandleA("user32.dll");
        if (!user32) user32 = LoadLibraryA("user32.dll");
        if (!user32) ExitProcess(1);
        static MSGBOXAAPI MessageBoxTimeout = (MSGBOXAAPI)GetProcAddress(user32, "MessageBoxTimeoutA");
        const char *text = ".";
        const char* caption = ".";
        if (MessageBoxTimeout) MessageBoxTimeout(NULL, caption, text, MB_ICONINFORMATION | MB_OK, 0, 5000);
        else MessageBoxA(NULL, caption, text, MB_ICONINFORMATION  | MB_OK);
    }

    return TRUE;
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        CreateThread(0, 0, (LPTHREAD_START_ROUTINE)MainThread, 0, 0, 0);
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}
